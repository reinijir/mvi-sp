# Predikce ATC tříd ze struktury léčiva pomocí konvolučních neuronových sítí
MI-MVI.16, semestrální práce  
Jiří Reiniš (reinijir)
  
## Stručné zadání

### Předzpracování dat:
- získat struktury ve formátu SMILES (+ labely)
- zakódovat SMILES do formy vhodné pro strojové učení: PNG obrázek struktury, cirkulární fingerprint

### Multi-label classification predikce
- jednoduchý random forests na cirkulárních fingerprintech
- konvoluční neuronové sítě

## Stažení dat:
1. **KEGG DRUG flatfile**: <ftp://ftp.genome.jp/pub/kegg/medicus/drug/drug>

## Pořadí notebooků:
1. preprocessing_1_prepare_data.ipynb 
2. preprocessing_2_encode_multiclass.ipynb
3. predict_ATC_random_forests.ipynb
4. balance_training_data.ipynb
5. predict_ATC_CNNs_v3.ipynb

Poslední notebook pouštěn na Google Colab.

## Prostředí (conda)
<https://gitlab.fit.cvut.cz/reinijir/mvi-sp/blob/master/environment.yml>